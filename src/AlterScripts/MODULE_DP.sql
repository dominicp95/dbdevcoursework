CREATE OR REPLACE EDITIONABLE TRIGGER  "BI_MODULE_DP" 
  before insert on "MODULE_DP"               
  for each row  
begin   
  if :NEW."MODULEID" is null then 
    select "MODULE_DP_SEQ".nextval into :NEW."MODULEID" from sys.dual; 
  end if; 
end; 

/
ALTER TRIGGER  "BI_MODULE_DP" ENABLE
/